import Algorithmia
from library.library.main import foo
# API calls will begin at the apply() method, with the request body passed as 'input'
# For more details, see algorithmia.com/developers/algorithm-development/languages
def apply(input):
    # This is a test comment, if your downstream dependency "library" is updated,
    # trigger a new pipelines job by commiting work, or by clicking "rerun" on the latest sucessful deploy.
    return foo(input) + "**"


if __name__ == "__main__":
    input = "algorithmia"
    result = apply(input)
    print(result)